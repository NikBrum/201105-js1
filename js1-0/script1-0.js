// JS 1.0
// JavaScript

"use strict";
/* print conversion table
   fahrenheit to celsius */


    const f2c = function () {
        let fahr;
        let celsius;

        let lower = 0;                       // define constants
        let upper = 300;
        let step = 20;

        let s = '<table>';
        s += '<tr><th>Fahrenheit</th><th>Celsius</th></tr>';
        fahr = lower;
        while (fahr <= upper) {
            celsius = 5 / 9 * (fahr - 32);   // the conversion formula
            s += `<tr><td>${fahr}</td><td>${celsius}</td></tr>`;
            fahr = fahr + step;
        }
        s += "</table>";
        return s;
    }

    const c2f = function () {
        let fahr;
        let celsius;

        let lower = 0;                       // define constants
        let upper = 300;
        let step = 20;

        let s = '<table>';
        s += '<tr><th>Celsius</th><th>Fahrenheit</th></tr>';
        celsius = lower;
        while (celsius <= upper) {
            fahr = celsius * 9 / 5 + 32;   // the conversion formula
            s += `<tr><td>${celsius}</td><td>${fahr}</td></tr>`;
            celsius = celsius + step;
        }
        s += "</table>";
        return s;
    }

window.addEventListener("load", f2c);

window.addEventListener("load", c2f);