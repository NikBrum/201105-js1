// JS 1.1
// JavaScript

"use strict";

const roll = function (foo) {    
    return Math.floor(Math.random() * foo) + 1;
}

let x = roll(6);
document.getElementById("dice").innerHTML = x;

window.addEventListener("load", roll);