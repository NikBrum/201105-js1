// JS 1.2
// JavaScript

"use strict";

const roll = function (foo) {    
    return Math.floor(Math.random() * foo) + 1;
}

let d1 = roll(6);
document.getElementById("d1").innerHTML = d1;
let d2 = roll(6);
document.getElementById("d2").innerHTML = d2;
let d3 = roll(6);
document.getElementById("d3").innerHTML = d3;
let d4 = roll(6);
document.getElementById("d4").innerHTML = d4;
let d5 = roll(6);
document.getElementById("d5").innerHTML = d5;

// Jeg havde svært ved at finde ud af hvordan jeg lavet det som et array
